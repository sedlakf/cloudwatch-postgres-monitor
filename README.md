# Monitor PostgreSQL replication with AWS CloudWatch

This tool allows you to monitor the lag of PostgreSQL WAL replication. It needs AWS
credentials (via EC2 instance role or via `aws configure`) and access to PostgreSQL
replica (via `--dsn` parameter).

## Installation

1. Install Pipenv
    - `sudo pip3 install pipenv`
2. Get the code
    - `sudo git clone https://gitlab.com/sedlakf/cloudwatch-postgres-monitor.git /opt/cloudwatch-postgres-monitor`
3. Install dependencies
    - `cd /opt/cloudwatch-postgres-monitor/ && sudo -H pipenv install`
4. Set up Systemd service
    - Create file `/etc/systemd/system/cloudwatch-postgres-monitoring.service`
    - An example is in this repo.
    - `sudo systemctl daemon-reload`
    - `sudo systemctl enable cloudwatch-postgres-monitoring.service`
    - `sudo systemctl start cloudwatch-postgres-monitoring.service`

In [AWS Cloudwatch Alarms](https://console.aws.amazon.com/cloudwatch/home?#alarmsV2:),
create an alarm that notifies you if this metric exceeds your threshold.

The metric is named `replication_lag` and is keyed by the dimensions InstanceID and Name
where the Name is taken from EC2 tags. You can optionally capture other tags as well.
