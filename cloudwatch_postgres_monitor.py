#!/usr/bin/env python3

"""Cloudwatch Postgresql replication monitor

Usage: cloudwatch_postgres_monitor.py --namespace=NAMESPACE [options]

Options:

-v --verbose            Be verbose.
--debug                 Be very verbose.

--capture-tags TAGS     Comma-separated list of instance tags that should be added
                        as dimensions to the metric. [default: Name]

--instance-id ID        Optional: specify instance ID to report. If you run this
                        on an EC2 instace, this tool will fetch it from AWS API.

--dsn DSN               PostgreSQL connection string, such as
                            dbname=test user=postgres...
                        If you can connect via pgsql without extra arguments, you
                        can leave the default value.
                        [default: ]

--period PERIOD         Metrics collection period (seconds) [default: 10]
"""

import logging
import time

import boto3
import docopt
import psycopg2
import requests


LOG = logging.getLogger(__name__)


def get_instance_id():
    resp = requests.get('http://169.254.169.254/latest/meta-data/instance-id')
    resp.raise_for_status()
    return resp.text


def main():
    args = docopt.docopt(__doc__)
    logging.basicConfig(level=logging.DEBUG if args['--verbose'] or args['--debug'] else logging.INFO)
    if not args['--debug']:
        logging.getLogger('botocore').setLevel(logging.INFO)
        logging.getLogger('urllib3').setLevel(logging.INFO)

    cloudwatch = boto3.client('cloudwatch')
    ec2 = boto3.client('ec2')

    instance_id = get_instance_id() if not args['--instance-id'] else args['--instance-id']
    capture_tags = args['--capture-tags'].split(',')

    connection = [None]

    def loop_iter():
        if connection[0] is None or connection[0].closed:
            connection[0] = psycopg2.connect(args['--dsn'])
            connection[0].autocommit = True

        dimensions = get_dimensions(ec2, capture_tags, instance_id)
        metric_data = list(get_metrics_values(connection[0]))
        for m in metric_data:
            m['Dimensions'] = dimensions
            LOG.debug('%s = %s', m['MetricName'], m['Value'])

        res = cloudwatch.put_metric_data(Namespace=args['--namespace'], MetricData=metric_data)
        LOG.debug('Cloudwatch responded with %s', res)

    LOG.info('Starting metrics collecting loop')
    loop(loop_iter, float(args['--period']))


def get_dimensions(ec2, capture_tags, instance_id):
    tags = ec2.describe_tags(Filters=[
        {'Name': 'resource-id', 'Values': [instance_id]},
        {'Name': 'key', 'Values': capture_tags}
    ])['Tags']

    dimensions = [{'Name': 'InstanceId', 'Value': instance_id }]
    return dimensions + [{'Name': t['Key'], 'Value': t['Value']} for t in tags]


def get_metrics_values(connection):
    with connection.cursor() as c:
        c.execute('SELECT extract(EPOCH FROM now() - pg_last_xact_replay_timestamp()) AS replication_lag;')
        row, = c.fetchall()
        replication_lag, = row
        yield {'MetricName': 'replication_lag', 'Value': replication_lag, 'Unit': 'Seconds'}


def loop(loop_iter, period):
    last_time = time.monotonic() - period

    while True:
        next_time = last_time + period
        wait_time = next_time - time.monotonic()
        last_time = next_time

        LOG.debug('Waiting %f seconds', wait_time)
        time.sleep(max(wait_time, 0))

        loop_iter()


if __name__ == '__main__':
    main()
